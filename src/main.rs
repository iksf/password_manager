extern crate chrono;
extern crate crypto;
extern crate gio;
extern crate glib;
extern crate gtk;
extern crate rand;
use std::thread;
use gtk::prelude::*;
use gtk::*;
use std::sync::mpsc::{channel, Receiver};
use std::rc::Rc;
use std::cell::RefCell;
use gio::prelude::*;
use std::env::args;
fn create_and_setup_view() -> TreeView {
    let tree = TreeView::new();
    tree.set_headers_visible(true);
    append_column(&tree, 0, "Passkey");
    append_column(&tree, 1, "Date + Time");
    append_column(&tree, 2, "Description");
    tree.set_hexpand(true);
    tree
}
fn append_column(tree: &TreeView, id: i32, title: &str) {
    let column = TreeViewColumn::new();
    let cell = CellRendererText::new();
    column.pack_start(&cell, true);
    column.set_expand(true);
    column.add_attribute(&cell, "text", id);
    column.set_title(title);
    tree.append_column(&column);
}
fn main_ui(application: &gtk::Application) {
    let window = gtk::ApplicationWindow::new(application);
    let output_bank: Rc<RefCell<Vec<(String, String)>>> = Rc::new(RefCell::new(Vec::new()));
    let time = gtk::Label::new(None);
    let (sender, receiver) = channel();
    GLOBAL.with({
        let time = time.clone();
        move |global| *global.borrow_mut() = Some((time, receiver))
    });
    thread::spawn(move || loop {
        std::thread::sleep(std::time::Duration::from_millis(100));
        let date = format!("{}", chrono::prelude::Utc::now());
        sender.send(date).unwrap();
        glib::idle_add(receive);
    });
    window.connect_delete_event({
        let window = window.clone();
        move |_, _| {
            window.destroy();
            Inhibit(false)
        }
    });
    let headerbox = gtk::HeaderBar::new();
    headerbox.set_show_close_button(true);
    headerbox.set_title("Password Manager");
    let save_button = gtk::Button::new();
    save_button.set_label("Save");
    headerbox.pack_end(&save_button);
    headerbox.pack_end(&time);
    let key_dialogue = gtk::Dialog::new_with_buttons(
        Some("Enter 4 to 56 digit password"),
        Some(&window),
        gtk::DialogFlags::empty(),
        &[("Ok", 0), ("Cancel", 1)],
    );
    let key_dialogue_box = key_dialogue.get_content_area();
    let key_dialogue_entry = gtk::Entry::new();
    key_dialogue_box.add(&key_dialogue_entry);
    key_dialogue.connect_response({
        let key_dialogue = key_dialogue.clone();
        let key_dialogue_entry = key_dialogue_entry.clone();
        let output_bank = Rc::clone(&output_bank);
        let window = window.clone();
        move |_, a| match a {
            1 => {
                key_dialogue.hide();
                key_dialogue_entry.set_buffer(&gtk::EntryBuffer::new(None));
            }
            0 => {
                let key = key_dialogue_entry.get_buffer().get_text();
                if key.len() < 4 || key.len() > 56 {
                    let popover = gtk::Popover::new(Some(&key_dialogue_entry));
                    popover.add(&gtk::Label::new("4-56 digits"));
                    popover.show_all();
                } else {
                    use crypto::symmetriccipher::BlockEncryptor;
                    let mut crypto = crypto::blowfish::Blowfish::new(key.as_bytes());
                    let output: Vec<([u8; 8], String)> = output_bank
                        .borrow()
                        .iter()
                        .map(|&(ref key, ref date)| {
                            let mut out: [u8; 8] = [0; 8];
                            crypto.encrypt_block(key.as_bytes(), &mut out);
                            (out, date.clone())
                        })
                        .collect();
                    let chooser_dialogue = gtk::FileChooserDialog::new(
                        Some("Choose bank or create new"),
                        Some(&window),
                        gtk::FileChooserAction::Save,
                    );
                    chooser_dialogue.add_buttons(
                        &[
                            ("Save", gtk::ResponseType::Ok.into()),
                            ("Cancel", gtk::ResponseType::Cancel.into()),
                        ],
                    );
                    chooser_dialogue.set_select_multiple(false);
                    chooser_dialogue.run();
                    let out = if !chooser_dialogue.get_filenames().is_empty() {
                        Some(chooser_dialogue.get_filenames()[0].clone())
                    } else {
                        None
                    };
                    chooser_dialogue.destroy();
                    key_dialogue.hide();
                    key_dialogue_entry.set_buffer(&gtk::EntryBuffer::new(None));
                    if let Some(path) = out {
                        use std::fs::File;
                        use std::io::Write;
                        let mut f = File::create(path).unwrap();
                        let out_buffer = output.iter().fold(
                            Vec::new(),
                            |mut v, &(ref data, ref date)| {
                                data.iter().for_each(|&x| v.push(x));
                                date.as_bytes().iter().for_each(|&x| v.push(x));
                                b"\n".iter().for_each(|&x| v.push(x));
                                v
                            },
                        );
                        f.write_all(&out_buffer[..]).expect("Failed to write");
                    }
                }
            }
            _ => (),
        }
    });
    save_button.connect_clicked({
        let key_dialogue = key_dialogue.clone();
        move |_| { key_dialogue.show_all(); }
    });
    let model = ListStore::new(
        &[
            String::static_type(),
            String::static_type(),
            String::static_type(),
        ],
    );
    let tree = create_and_setup_view();
    tree.set_model(Some(&model));
    window.set_titlebar(&headerbox);
    let grid = gtk::Grid::new();
    let top_grid = gtk::Grid::new();
    let entries = Rc::new(RefCell::new(
        (1..5)
            .map(|x| {
                let y = gtk::Entry::new();
                y.get_buffer().set_max_length(Some(2));
                y.set_hexpand(true);
                top_grid.attach(&y, x, 0, 1, 1);
                y
            })
            .collect::<Vec<_>>(),
    ));
    let buttons = {
        let mut buttons = Vec::new();
        let add = |x| {
            let y = gtk::Button::new();
                y.set_hexpand(true);
            top_grid.attach(&y, x, 0, 1, 1);
            y
        };
        buttons.push(add(0));
        buttons.push(add(6));
        buttons
    };
    buttons[0].set_label("Random");
    buttons[0].connect_clicked({
        let entries = Rc::clone(&entries);
        move |_| {
            use rand::distributions::{IndependentSample, Range};
            let range = Range::new(0, 100);
            let mut rng = rand::thread_rng();
            entries.borrow().iter().for_each(|x| {
                x.set_text(&{
                    let out = format!("{}", range.ind_sample(&mut rng));
                    if out.len() == 1 {
                        format!("0{}", out)
                    } else {
                        out
                    }
                });
            });
        }
    });
    buttons[1].set_label("Add");
    buttons[1].connect_clicked({
        let entries = Rc::clone(&entries);
        let model = model.clone();
        let window = window.clone();
        let output_bank = Rc::clone(&output_bank);
        move |_| if entries.borrow().iter().all(|x| if x.get_buffer()
            .get_text()
            .len() == 2
        {
            x.get_buffer().get_text().chars().all(
                |y| match y.to_digit(10) {
                    Some(_) => true,
                    _ => {
                        let popover = gtk::Popover::new(Some(x));
                        popover.add(&gtk::Label::new("Must be fully filled out with numbers"));
                        popover.show_all();
                        false
                    }
                },
            )
        } else {
            let popover = gtk::Popover::new(Some(x));
            popover.add(&gtk::Label::new("Must be fully filled out with numbers"));
            popover.show_all();
            false
        })
        {
            let prompt_for_description = gtk::Dialog::new_with_buttons(
                Some("Enter description"),
                Some(&window),
                gtk::DialogFlags::empty(),
                &[("Ok", 0)],
            );

            let prompt_container = prompt_for_description.get_content_area();
            let prompt_entry = gtk::Entry::new();
            prompt_container.add(&prompt_entry);
            prompt_for_description.show_all();
            prompt_for_description.connect_response({
                let output_bank = output_bank.clone();
                let model = model.clone();
                let entries = entries.clone();
                let prompt_entry = prompt_entry.clone();
                let prompt_for_description = prompt_for_description.clone();
                move |_, a| {match a {
                    0 => {
                        let string = &*entries
                            .borrow()
                            .iter()
                            .map(|x| x.get_buffer().get_text())
                            .collect::<String>();
                        let date = format!("{}", chrono::prelude::Utc::now());
                        let description = prompt_entry.get_buffer().get_text();
                        output_bank.borrow_mut().push(
                            (string.to_owned(), date.to_owned()),
                        );
                        model.insert_with_values(
                            None,
                            &[0, 1, 2],
                            &[&string, &date, &description],
                        );

                    }
                    _ => (),
                };
                prompt_for_description.destroy();
                }
            });
        }
    });
    grid.attach(&top_grid, 0, 0, 1, 1);
    grid.attach(&tree, 0, 1, 1, 1);
    window.add(&grid);
    window.show_all();
}

fn receive() -> glib::Continue {
    GLOBAL.with(|global| if let Some((ref buf, ref rx)) = *global.borrow() {
        if let Ok(text) = rx.try_recv() {
            buf.set_label(&text);
        }
    });
    glib::Continue(false)
}
thread_local!(
    static GLOBAL: RefCell<Option<(gtk::Label, Receiver<String>)>> = RefCell::new(None)
);
fn main() {
    let application = gtk::Application::new(
        "com.kieran.password_manager",
        gio::ApplicationFlags::empty(),
    ).expect("Initialisation failed...");
    application.connect_startup(move |app| { main_ui(app); });
    application.connect_activate(|_| {});
    application.run(&args().collect::<Vec<_>>());
}
